import os
import unittest
import tempfile
import json

from myrm import mrm
from myrm import config
from myrm import trash_manager

class TestRm(unittest.TestCase):

    def setUp(self):
        self.test_trash = tempfile.mkdtemp()

    def test_rm_file(self):
        ok, i = True, 0

        while i < 10:
            try:
                with tempfile.NamedTemporaryFile() as tmp:
                    trash_manager.remove(tmp.name, False, True, False)
                ok = ok and not os.path.exists(tmp.name)
            except:
                pass
            i += 1

        self.assertFalse(not ok, 'ERROR')


    def test_rm_file_trash(self):
        conf = config.Config()
        ok = True
        try:
            trashes = os.listdir(conf.trash)
            for i in trashes:
                trash_manager.real_remove(i, False, True, False)
        except:
            pass

        self.assertFalse(not ok, 'ERROR')

    def test_change_size_trash(self):
        options = {}
        options['size'] = 5000
        conf = config.Config(**options)
        self.assertFalse (conf.size != 5000, 'ERROR')

    def test_change_policy_trash(self):
        options = {}
        options['policy'] = 'time'
        conf = config.Config(**options)
        self.assertFalse(conf.policy != 'time', 'ERROR')

    def test_change_day_trash(self):
        options = {}
        options['day'] = 1
        conf = config.Config(**options)
        self.assertFalse(conf.day != 1, 'ERROR')

    def test_read_from_cfg(self):
        options = {}
        options['json_f'] = False
        conf = config.Config(**options)
        self.assertFalse(conf.json_f == True, 'ERROR')

    def test_read_from_non_stand(self):
        conf1 = config.Config()
        options = {}
        options['path_rm_conf_json'] = "/Users/artemastasenok/Documents/forlab2/rmconfigs.json"
        conf2 = config.Config(**options)
        self.assertFalse(conf1 == conf2, 'ERROR')

    def test_dryrun_mod(self):
        ok, i = True, 0
        dry_run = True
        while i < 10:
            try:
                with tempfile.NamedTemporaryFile() as tmp:
                    trash_manager.remove(tmp.name, dry_run, True, False)
                ok = ok and not os.path.exists(tmp.name)
            except:
                pass
            i += 1

        self.assertFalse(not ok, 'ERROR')

    def test_recover(self):
        ok,i = True,0
        while i < 10:
            try:
                with tempfile.NamedTemporaryFile() as tmp:
                    mrm.remove(tmp.name, False, True, False)
                    mrm.recover(tmp.name, False, True, False)
                ok = ok and os.path.exists(tmp.name)
            except:
                pass
            i += 1

        self.assertFalse(not ok, 'ERROR')


if __name__ == '__main__':
    unittest.main()