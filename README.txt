MYRMTOOL
Small UNIX OS utilite providing removing/restoring your files and folders with file storage support. Developed on Python 2.7

Usage guide:

To access removing facilities, run the next command in the terminal:
myrm [arguments] [path to file] 
If run in default mode (e.g. without flags), removes files and not empty directories to the file storage folder.

to remove not empty directories, specify -r flag:
myrm -r [path to file]

to restore a file, specify -l flag:
myrm -l [name file]

To remove from the Recycle Bin, specify -c flag:
myrm -c [name file]

to display the log on the console screen, specify -log flag:
myrm -log

for removal by regular expression, specify -reg flag:
myrm -reg [regular]

To display the contents of the Recycle Bin, specify -v flag:
myrm -v

To change the size of the basket, specify —-size flag:
myrm —-size [size]

To change the day for policy of the basket, specify -—day flag:
myrm —-day [day]

To change police, specify —- policy flag:
myrm —police [policy]

To see the rest facilities, run:
myrm --help


Installation guide
Run:
Assemble a distribution package
$ python setup.py sdist

Create environment env:
virtualenv env
The command will create the env directory inside our project and install python, pip and distribute there. We will install our project in it.
$ ./env/bin/python setup.py install

Reinstall the module in our environment and test the work of the created script (for this we will have to activate our environment):

$ ./env/bin/python setup.py install
$ source ./env/bin/activate

Moodules description
1) -mrm.py

Represents application's entry point. The place where all the main methods are called.

2) -config.py

The module provides creating/saving/loading configuration files.

3) -setup.py

Specifies such information as version, software name, description and entry points for the application after installation. Hence the name.

4)-trash_manager.py 


Describes the basic functions for working with the basket: recovery, removal, cleaning