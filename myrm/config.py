# module config
"""

"""

import ConfigParser
import json
import os
import logging
import argparse
import sys


class Config(object):
    PATH_RM_CONFIG_JSON = os.path.join(os.path.expanduser('~'),
                                       'Documents/untitled1/rmconfigs.json')
    PATH_RM_CONFIG_CFG = os.path.join(os.path.expanduser('~'), 'Documents/untitled1/rmconfigs.cfg')
    DAY_FOR_DELETE = 6
    MAX_SIZE = 1000
    PATH_TRASH = os.path.join(os.path.expanduser('~'), 'Documents/forlab2/trash')
    POLICY = "full"
    PATH_LOG = os.path.join(os.path.expanduser('~'), 'Documents/forlab2/rmlogs.log')
    JSON_FORMAT = True

    def __init__(self, json_format=JSON_FORMAT, path_rm_conf_json=PATH_RM_CONFIG_JSON,
                 path_conf_cfg=PATH_RM_CONFIG_CFG, policy=POLICY, path_trash=PATH_TRASH,
                 size=MAX_SIZE, day_for_delete=DAY_FOR_DELETE, path_log=PATH_LOG):
        """     
        Initialize configuration function
        """

        self.path = path_trash
        self.json_f = json_format
        self.path_rm_conf_json = path_rm_conf_json
        self.path_conf_cfg = path_conf_cfg
        self.standart_configs = {"policy": policy,
                                 "path": path_trash,
                                 "size": size,
                                 "day": day_for_delete
                                 }
        self.configs = ConfigParser.RawConfigParser()

        if not os.path.exists(self.path_rm_conf_json):
            self.create_configs()

        if not os.path.exists(self.path_conf_cfg):
            self.create_configs()

        self.configs_json = json.load(open(self.path_rm_conf_json, 'r'))
        self.configs.read(self.path_conf_cfg)

        if json_format:
            self.policy = self.configs_json['policy']
            self.trash = self.configs_json['path']
            self.size = self.configs_json['size']
            self.day = self.configs_json['day']
        else:
            self.policy = self.configs.get('CONFIGS', 'Policy')
            self.trash = self.configs.get('CONFIGS', 'Path')
            self.size = self.configs.get('CONFIGS', 'Size')
            self.day = self.configs.get('CONFIGS', 'Day')

        self.list_old = os.path.join(self.trash, "_____111_1")

        if self.size != size and size != self.MAX_SIZE:
            self.size = size

        if self.day != day_for_delete and day_for_delete != self.DAY_FOR_DELETE:
            self.day = day_for_delete

        if self.policy != policy and policy != self.POLICY:
            self.policy = policy

    def create_configs(self, policy=POLICY, path=PATH_TRASH,
                       size=MAX_SIZE, day=DAY_FOR_DELETE):
        """
        The function of creating a config, if it does not exist
        """

        if not os.path.exists(self.path_rm_conf_json):
            json.dump(self.standart_configs, open(self.path_rm_conf_json, 'w'))

        if not os.path.exists(self.path_conf_cfg):
            self.configs.add_section('CONFIGS')
            self.configs.set('CONFIGS', 'Policy', policy)
            self.configs.set('CONFIGS', 'Path', path)
            self.configs.set('CONFIGS', 'Size', size)
            self.configs.set('CONFIGS', 'Day', day)
            with open(self.path_conf_cfg, 'w') as configfile:
                self.configs.write(configfile)

    def write_configs(self, new_path, new_policy, new_size, new_day):
        """
        Function rewrite config
        """

        if new_path is not None:
            self.configs.set('CONFIGS', 'Path', os.path.join(os.path.abspath(new_path), 'trash'))
            self.configs_json['path'] = os.path.join(new_path, 'trash')

        if new_policy is not None:
            self.configs.set('CONFIGS', 'Policy', new_policy)
            self.configs_json['policy'] = new_policy;

        if new_size is not None:
            self.configs.set('CONFIGS', 'Size', new_size)
            self.configs_json['size'] = new_size;

        if new_day is not None:
            self.configs.set('CONFIGS', 'Day', new_day)
            self.configs_json['day'] = new_day;

        with open(self.path_conf_cfg, 'w') as configfile:
            self.configs.write(configfile)

        json.dump(self.configs_json, open(self.path_rm_conf_json, 'w'))
