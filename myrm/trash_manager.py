# module trash_manager
"""
This module provides work with Trash from console. Ypu can use one Trash in one time.
Trash create by class Trash from module smart_rm_action.
Available function:
    1. delete_by_size_policy() --> delete files from trash by size
    2. delete_by_date_policy() --> delete files from trash by date
    3. delete_by_regex(path, regex) --> delete files from subdir
        (path_to_subdir = path) be regex
    4. delete_with_confirmation(files) --> get user choice to delete file
    5. delete_permanently(files) --> delete files from computer(y can't restore it)
    6. remove_to_trash(files) --> delete files to Trash(you can restore it)
    7. restore_files(files) --> restore files from Trash
If you want to create Trash and change Trash settings, use module config.
"""
import os
import json
import logging
import re
import datetime
import config
import shutil
from ask_user import ask

LOGGER = logging.getLogger('application')
CONF = config.Config()


def remove(name, dry_run=False, force=False, silent=False):
    """
    Function deletes the file in the trash
    """
    if os.path.exists(name):
        if dry_run:
            just_name = name.split("/")[-1]
            if not force:
                ask("Are you sure you want to perform " + just_name + " y/yes?")
            if not silent:
                LOGGER.info(just_name + " can be Moved to the trash")
        else:
            just_name = name.split("/")[-1]
            if os.path.exists(CONF.list_old):
                with open(CONF.list_old, "r") as file:
                    old_path = json.load(file)
                while just_name in old_path:
                    just_name = just_name + "_"
            else:
                old_path = {}
            if not force:
                if ask("Are you sure you want to perform " + just_name + "  y/yes?"):
                    os.rename(name, os.path.join(CONF.trash, just_name))
                    old_path[just_name] = name
                    if not silent:
                        LOGGER.info(just_name + " moved to the trash")
                elif not silent:
                    LOGGER.info(just_name + " don't moved to the trash")
            else:
                os.rename(name, os.path.join(CONF.trash, just_name))
                old_path[just_name] = name
                if not silent:
                    LOGGER.info(just_name + " moved to the trash")
            with open(CONF.list_old, "w") as file:
                json.dump(old_path, file)
    else:
            LOGGER.error("File not found")


def remove_list(files, dryrun=False, force = False, silent = False):
    """
    Function for optimizing deletion work
    """
    for name in files:
        remove(os.path.abspath(name), dryrun, force, silent)


def recover(name, dryrun=False, force=False, silent=False):
    """
    The function restores the file along the old path  
    """
    if os.path.exists(CONF.list_old):
        with open(CONF.list_old, "r") as file:
            old_path = json.load(file)
        if name in old_path:
            if dryrun:
                if not force:
                    ask("Are you sure you want to perform " + name + " y/yes?")
                if not silent:
                    LOGGER.info(name + " can be recover from the recycle bin")
            else:
                if not force:
                    if ask("Are you sure you want to perform " + name + " y/yes?"):
                        os.rename(os.path.join(CONF.trash, name), old_path[name])
                        del(old_path[name])
                        if not silent:
                            LOGGER.info(name + " recover from the recycle bin")
                    elif not silent:
                        LOGGER.info(name + " File not recovered")
                else:
                    os.rename(os.path.join(CONF.trash, name), old_path[name])
                    del (old_path[name])
                    if not silent:
                        LOGGER.info(name + " recover from the recycle bin")
            with open(CONF.list_old, "w") as file:
                json.dump(old_path, file)
        elif not silent:
            LOGGER.error("File not found")


def real_remove(name, dryrun=False, force=False, silent=False):
    """
    Function removes the file in full from the recycle bin
    """
    if os.path.exists(CONF.list_old):
        with open(CONF.list_old, "r") as file:
            old_path = json.load(file)
        if name in old_path:
            if dryrun:
                if not force:
                    ask("Are you sure you want to perform " + name + " y/yes?")
                if not silent:
                    LOGGER.info(name + " can be cleared from the recycle bin")
            else:
                if os.path.isfile(os.path.join(CONF.trash,name)) or os.path.islink(os.path.join(CONF.trash,name)):
                    if not force:
                        if ask("Are you sure you want to perform " + name + " y/yes?"):
                            os.remove(os.path.join(CONF.trash, name))
                            del (old_path[name])
                            if not silent:
                                LOGGER.info(name + " clear from trash")
                        else:
                            if not silent:
                                LOGGER.info(name + " don't clear from trash")
                    else:
                        os.remove(os.path.join(CONF.trash, name))
                        del (old_path[name])
                        if not silent:
                            LOGGER.info(name + " clear from trash")
                        else:
                            if not silent:
                                LOGGER.info(name + " don't clear from trash")
                else:
                    if not force:
                        if ask("Are you sure you want to perform " + name + " y/yes?"):
                            shutil.rmtree(os.path.join(CONF.trash, name))
                            del (old_path[name])
                            if not silent:
                                LOGGER.info(name + " clear from trash")
                        elif not silent:
                            LOGGER.info(name + " clear from trash")
            with open(CONF.list_old, "w") as file:
                json.dump(old_path, file)
        else:
            if not silent:
                LOGGER.error("File not found")


def recycle_policy(policy, day, size):
    """
        The policy deletion function, 2 policies are available, by size, 
        if the specified basket size is exceeded, and by date, 
        the recycle bin will be cleaned on a specific day
    """
    if policy == "full" or policy == "time":
        if policy == "full":
            real_size = os.path.getsize(CONF.trash)
            if os.path.exists(CONF.list_old):
                with open(CONF.list_old, "r") as file:
                    old_path = json.load(file)
                if real_size > int(size):
                    for name in old_path:
                        real_remove(name, dryrun=False, force=True, silent=False)
        if policy == "time":
            if datetime.datetime.today().isoweekday() == day:
                if os.path.exists(CONF.list_old):
                    with open(CONF.list_old, "r") as file:
                        old_path = json.load(file)
                for name in old_path:
                    real_remove(name, False, True, False)
    else:
        LOGGER.error("ConfigError: configurations have not been changed, is wrong policy")


def regular_remove(pattern):
    """
    Delete function in the regular expression
    """
    reg = os.listdir(".")
    for name in reg:
        if re.findall(pattern, name):
            print name
            remove(os.path.abspath(name))

